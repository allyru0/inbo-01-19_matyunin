from converter import convert

number = 100

def test_mmToSm():
    assert convert(number, 'mm', 'sm') == 10

def test_mmToDm():
    assert convert(number, 'mm', 'dm') == 1

def test_mmToM():
    assert convert(number, 'mm', 'm') == 0.1

def test_mmToKm():
    assert convert(1000, 'mm', 'km') == 0.001

def test_smToMm():
    assert convert(number, 'sm', 'mm') == 1000

def test_smToDm():
    assert convert(number, 'sm', 'dm') == 10

def test_mmToM():
    assert convert(number, 'sm', 'm') == 1

def test_smToKm():
    assert convert(number, 'sm', 'km') == 0.001

def test_dmToMm():
    assert convert(number, 'dm', 'mm') == 10000

def test_dmToSm():
    assert convert(number, 'dm', 'sm') == 1000

def test_dmToM():
    assert convert(number, 'dm', 'm') == 10

def test_dmToKm():
    assert convert(number, 'dm', 'km') == 0.01

def test_mToMm():
    assert convert(number, 'm', 'mm') == 100000

def test_mToSm():
    assert convert(number, 'm', 'sm') == 10000

def test_mToDm():
    assert convert(number, 'm', 'dm') == 1000

def test_mToKm():
    assert convert(number, 'm', 'km') == 0.1

def test_kmToMm():
    assert convert(number, 'km', 'mm') == 100000000

def test_kmToSm():
    assert convert(number, 'km', 'sm') == 10000000

def test_kmToDm():
    assert convert(number, 'km', 'dm') == 1000000

def test_kmToM():
    assert convert(number, 'km', 'm') == 100000

def test_errorNumber():
    assert convert('gger', 'm', 'mm') == 'error'

def test_errorFromUnit():
    assert convert(number, 'mmm', 'm') == 'error'

def test_errorToUnit():
    assert convert(number, 'm', 'smm') == 'error'