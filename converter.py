def mmToSm(value):
    return value*0.1

def mmToDm(value):
    return value*0.01

def mmToM(value):
    return value*0.001

def mmToKm(value):
    return value*0.000001

def smToMm(value):
    return value*10

def smToM(value):
    return value*0.01

def smToDm(value):
    return value*0.1

def smToKm(value):
    return smToM(value)*0.001

def dmToMm(value):
    return value*100

def dmToSm(value):
    return value*10

def dmToM(value):
    return value*0.1

def dmToKm(value):
    return dmToM(value)*0.001

def mToMm(value):
    return value*1000

def mToSm(value):
    return value*100

def mToDm(value):
    return value*10

def mToKm(value):
    return value*0.001

def kmToMm(value):
    return value*1000000

def kmToSm(value):
    return value*100000

def kmToDm(value):
    return value*10000

def kmToM(value):
    return value*1000

def convert(value, fromUnit, toUnit):
    with open('test.txt', 'a') as test_file:
        if not isinstance(value, int):
            test_file.write(f'{value} must be Integer!\n')
            return 'error'
        if fromUnit != 'mm' and fromUnit != 'sm' and fromUnit != 'dm' and fromUnit != 'm' and fromUnit != 'km':
            test_file.write(f'Wrong first unit!\n')
            return 'error'
        if toUnit != 'mm' and toUnit != 'sm' and toUnit != 'dm' and toUnit != 'm' and toUnit != 'km':
            test_file.write(f'Wrong second unit!\n')
            return 'error'

        result = 0

        if fromUnit == 'mm':
            if toUnit == 'sm':
                result = mmToSm(value)
            if toUnit == 'dm':
                result = mmToDm(value)
            if toUnit == 'm':
                result = mmToM(value)
            if toUnit == 'km':
                result = mmToKm(value)
        if fromUnit == 'sm':
            if toUnit == 'mm':
                result = smToMm(value)
            if toUnit == 'dm':
                result = smToDm(value)
            if toUnit == 'm':
                result = smToM(value)
            if toUnit == 'km':
                result = smToKm(value)
        if fromUnit == 'dm':
            if toUnit == 'mm':
                result = dmToMm(value)
            if toUnit == 'sm':
                result = dmToSm(value)
            if toUnit == 'm':
                result = dmToM(value)
            if toUnit == 'km':
                result = dmToKm(value)
        if fromUnit == 'm':
            if toUnit == 'mm':
                result = mToMm(value)
            if toUnit == 'dm':
                result = mToDm(value)
            if toUnit == 'sm':
                result = mToSm(value)
            if toUnit == 'km':
                result = mToKm(value)
        if fromUnit == 'km':
            if toUnit == 'mm':
                result = kmToMm(value)
            if toUnit == 'dm':
                result = kmToDm(value)
            if toUnit == 'm':
                result = kmToM(value)
            if toUnit == 'sm':
                result = kmToSm(value)
        test_file.write(f'{value} {fromUnit} = {result} {toUnit}\n')
    return result
