from converter import convert
try:
   number = int(input('Введите число, которое необходимо перевести: '))
except Exception:
   print('Введено некорректное число')
   exit(1)
fromUnit = input('Введите единицы числа: ')
if fromUnit != 'mm' and fromUnit != 'sm' and fromUnit != 'dm' and fromUnit != 'm' and fromUnit != 'km':
   print('Введена некорректная единица измерения!')
   exit(1)
toUnit = input('Введите единицы, в которые необходимо перевести: ')
if toUnit != 'mm' and toUnit != 'sm' and toUnit != 'dm' and toUnit != 'm' and toUnit != 'km':
   print('Введена некорректная единица измерения!')
   exit(1)

value = convert(number, fromUnit, toUnit)
value = "%.6f" % (value)

print(f'{number} {fromUnit} = {value} {toUnit}')